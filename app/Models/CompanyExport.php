<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class CompanyExport extends Model implements HasMedia
{
    use HasFactory, Notifiable, HasRoles, InteractsWithMedia;

    protected $fillable = ['name','address','telephone'];
    protected $table = 'company_export';

    /**
     * @return \Spatie\MediaLibrary\MediaCollections\Models\Media|null
     */

    public function getLogoAttribute(){
        return $this->getFirstMedia('logo');
    }


}
