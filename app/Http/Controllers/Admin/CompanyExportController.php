<?php

namespace App\Http\Controllers\Admin;

use App\Library\FlashMessages;
use App\Models\CompanyExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyExportController extends AdminBaseController
{
    use FlashMessages;
    protected $diskName = 'company-export';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:company-export-list|company-export-create|company-export-edit|company-export-delete', ['only' => ['index','store']]);
        $this->middleware('permission:company-export-create', ['only' => ['create','store']]);
        $this->middleware('permission:company-export-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:company-export-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companyExportList = CompanyExport::orderBy('id','DESC')->paginate(5);

        return view('admin.company_export.index',compact('companyExportList'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company_export.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $lastRecord = CompanyExport::query()
            ->where('code','like','CE%')
            ->orderBy('code','desc')
            ->first();

        $companyExport = CompanyExport::create($request->all());
        $code = $lastRecord ? (int)str_replace("CE","",$lastRecord->code) : 0;
        $companyExport->code = is_null($lastRecord) ? 'CE0001' : 'CE'.sprintf('%04d', $code + 1);

        if($request->hasFile('logo') && $request->file('logo')->isValid()){
            $companyExport->addMediaFromRequest('logo')
                ->toMediaCollection('logo',$this->diskName);
        }

        $companyExport->save();

        return redirect()->route('company-export.index')
            ->with('success','Company export created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = CompanyExport::find($id);
        return view('admin.company_export.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = CompanyExport::find($id);
        return view('admin.company_export.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $company = CompanyExport::find($id);
        $company->fill($request->all());

        if($request->hasFile('logo') && $request->file('logo')->isValid()){
//            $company->deletePreservingMedia('logo');
//            $company->save();
            $company->clearMediaCollection('logo');
            $company->addMediaFromRequest('logo')
                ->toMediaCollection('logo',$this->diskName);
        }

        $company->save();

        return redirect()->route('company-export.index')
            ->with('success','Company export updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("company_export")->where('id', $id)->delete();
        self::success('Company export deleted successfully.');
        return response('Company export deleted successfully.', 200);
    }
}
