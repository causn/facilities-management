<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    public function index(){
        $locale = str_replace("-",  "_", app()->getLocale());
        if (!file_exists(public_path("packages/barryvdh/elfinder/js/i18n/elfinder.".$locale.".js"))) {
            $locale = "vi";
        }
        return view('admin.media.index',['locale'=>$locale]);
    }
}
