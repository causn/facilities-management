-- --------------------------------------------------------
-- Host:                         sql12.freemysqlhosting.net
-- Server version:               5.5.62-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table sql12609096.company_export
CREATE TABLE IF NOT EXISTS `company_export` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.company_export: ~6 rows (approximately)
INSERT INTO `company_export` (`id`, `name`, `code`, `address`, `telephone`, `created_at`, `updated_at`) VALUES
	(3, 'Abilive VietName', 'CE0001', 'TID Center, 4 P. Liễu Giai, Cống Vị, Ba Đình, Hà Nội, Việt Nam', '0942823696', '2023-03-29 17:37:37', '2023-03-29 17:37:37'),
	(4, 'adamo', NULL, 'to huu, ha noi, viet nam', '0942823696', '2023-03-31 11:59:58', '2023-03-31 11:59:58'),
	(5, 'adamo', 'CE0002', 'to huu, ha noi, viet nam', '0942823696', '2023-03-31 12:00:30', '2023-03-31 12:00:30'),
	(6, 'joomfashion', 'CE0003', NULL, NULL, '2023-03-31 12:52:39', '2023-03-31 12:52:39'),
	(7, '1office', NULL, 'TID Center, 4 P. Liễu Giai, Cống Vị, Ba Đình, Hà Nội, Việt Nam', '0942823696', '2023-03-31 15:16:52', '2023-03-31 15:16:52'),
	(8, '1office', 'CE0004', 'TID Center, 4 P. Liễu Giai, Cống Vị, Ba Đình, Hà Nội, Việt Nam', '0942823696', '2023-03-31 15:17:34', '2023-03-31 15:17:35');

-- Dumping structure for table sql12609096.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table sql12609096.media
CREATE TABLE IF NOT EXISTS `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `manipulations` longtext COLLATE utf8mb4_unicode_ci,
  `custom_properties` longtext COLLATE utf8mb4_unicode_ci,
  `generated_conversions` longtext COLLATE utf8mb4_unicode_ci,
  `responsive_images` longtext COLLATE utf8mb4_unicode_ci,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_uuid_unique` (`uuid`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`),
  KEY `media_order_column_index` (`order_column`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.media: ~6 rows (approximately)
INSERT INTO `media` (`id`, `model_type`, `model_id`, `uuid`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `conversions_disk`, `size`, `manipulations`, `custom_properties`, `generated_conversions`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
	(1, 'App\\Models\\CompanyExport', 3, '17f52396-a199-4561-a127-09ccbbc5daa8', 'logo', 'img_ov1', 'img_ov1.jpg', 'image/jpeg', 'company-export', 'media', 12560, '[]', '[]', '[]', '[]', 1, '2023-03-29 17:37:37', '2023-03-29 17:37:37'),
	(2, 'App\\Models\\CompanyExport', 4, '603f1843-bbd0-4e71-95be-d25c497148ff', 'logo', 'drawing_room_01', 'drawing_room_01.png', 'image/png', 'media', 'media', 8374, '[]', '[]', '[]', '[]', 2, '2023-03-31 11:59:59', '2023-03-31 11:59:59'),
	(3, 'App\\Models\\CompanyExport', 5, 'b7016199-5808-48c1-9ca3-63603c7ff6d4', 'logo', 'drawing_room_01', 'drawing_room_01.png', 'image/png', 'media', 'media', 8374, '[]', '[]', '[]', '[]', 3, '2023-03-31 12:00:30', '2023-03-31 12:00:30'),
	(4, 'App\\Models\\CompanyExport', 6, '2123d829-7608-4a9f-b475-edbe1b182095', 'logo', 'img_room_02', 'img_room_02.jpg', 'image/jpeg', 'media', 'media', 125043, '[]', '[]', '[]', '[]', 4, '2023-03-31 12:52:39', '2023-03-31 12:52:39'),
	(5, 'App\\Models\\CompanyExport', 7, '7fb5138a-ae39-490f-97b6-06aa1200f032', 'logo', 'img_sample06', 'e237465e516cd2d2fb6877177d50a8ba.jpg', 'image/jpeg', 'company-export', 'company-export', 124927, '[]', '[]', '[]', '[]', 5, '2023-03-31 15:16:53', '2023-03-31 15:16:53'),
	(6, 'App\\Models\\CompanyExport', 8, 'ef601bf1-a600-415f-8116-68eabb5c0346', 'logo', 'img_sample06', '7b208aeb2edd7a981402c780d79929ca.jpg', 'image/jpeg', 'company-export', 'company-export', 124927, '[]', '[]', '[]', '[]', 6, '2023-03-31 15:17:34', '2023-03-31 15:17:34');

-- Dumping structure for table sql12609096.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.migrations: ~11 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2023_03_14_044834_create_permission_tables', 1),
	(6, '2023_03_14_062644_create_products_table', 1),
	(7, '2023_03_28_073738_drop_product_table', 1),
	(8, '2023_03_28_074257_create_company_export_table', 1),
	(9, '2023_03_29_043055_create_media_table', 1),
	(10, '2023_03_29_071443_drop_column_logo_in_company_export_table', 1),
	(11, '2023_03_29_072129_modify_column_code_in_company_export_table', 1);

-- Dumping structure for table sql12609096.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.model_has_permissions: ~0 rows (approximately)

-- Dumping structure for table sql12609096.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.model_has_roles: ~1 rows (approximately)
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\Models\\User', 1);

-- Dumping structure for table sql12609096.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.password_resets: ~0 rows (approximately)

-- Dumping structure for table sql12609096.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.permissions: ~12 rows (approximately)
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'role-list', 'web', '2023-03-29 17:14:09', '2023-03-29 17:14:09'),
	(2, 'role-create', 'web', '2023-03-29 17:14:10', '2023-03-29 17:14:10'),
	(3, 'role-edit', 'web', '2023-03-29 17:14:10', '2023-03-29 17:14:10'),
	(4, 'role-delete', 'web', '2023-03-29 17:14:11', '2023-03-29 17:14:11'),
	(5, 'company-export-list', 'web', '2023-03-29 17:14:11', '2023-03-29 17:14:11'),
	(6, 'company-export-create', 'web', '2023-03-29 17:14:11', '2023-03-29 17:14:11'),
	(7, 'company-export-edit', 'web', '2023-03-29 17:14:12', '2023-03-29 17:14:12'),
	(8, 'company-export-delete', 'web', '2023-03-29 17:14:12', '2023-03-29 17:14:12'),
	(9, 'user-list', 'web', '2023-03-29 17:14:12', '2023-03-29 17:14:12'),
	(10, 'user-create', 'web', '2023-03-29 17:14:13', '2023-03-29 17:14:13'),
	(11, 'user-edit', 'web', '2023-03-29 17:14:13', '2023-03-29 17:14:13'),
	(12, 'user-delete', 'web', '2023-03-29 17:14:14', '2023-03-29 17:14:14');

-- Dumping structure for table sql12609096.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.personal_access_tokens: ~0 rows (approximately)

-- Dumping structure for table sql12609096.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.roles: ~1 rows (approximately)
INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'web', '2023-03-29 17:14:14', '2023-03-29 17:14:14');

-- Dumping structure for table sql12609096.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.role_has_permissions: ~12 rows (approximately)
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1);

-- Dumping structure for table sql12609096.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table sql12609096.users: ~1 rows (approximately)
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'sinhcautb', 'sinhcautb.ictu@gmail.com', NULL, '$2y$10$8PepwgMkSBa6RWblk7R.GuX1/XUP6VgDSyuWJSAjIBP7XFIpbtrMS', NULL, '2023-03-29 17:14:14', '2023-03-29 17:14:14');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
