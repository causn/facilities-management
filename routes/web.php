<?php

use App\Http\Controllers\Admin\CompanyExportController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DashBoardController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ProfileController;
use League\Glide\ServerFactory;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Admin\MediaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//frontend
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

//backend
Route::group(['prefix' => 'admin','middleware' => ['admin']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('company-export', CompanyExportController::class);
    Route::get('/dashboard', [DashBoardController::class, 'index'])->name('dashboard');
    Route::get('profile',[ProfileController::class,'index'])->name('profile');
    Route::post('profile/{user}',[ProfileController::class,'update'])->name('profile.update');
    Route::get('glide/{path}', function($path){
        $server = ServerFactory::create([
            'source' => app('filesystem')->disk('media')->getDriver(),
            'cache' => storage_path('media'),
        ]);
        return $server->getImageResponse($path, Request::query());
    })->where('path', '.+');
    Route::get('/media', [MediaController::class, 'index'])->name('media');
});

