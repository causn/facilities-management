<!DOCTYPE html>
<html lang="en">
@include("layouts.top")
<body class="">
<!-- Extra details for Live View on GitHub Pages -->
<div class="wrapper ">
    @include("layouts.left-slide")
    <div class="main-panel">
        @include("layouts.top-slide")
        <div class="content">
            @yield('content')
        </div>
        @include("layouts.footer")
    </div>
</div>
</body>
@include("layouts.bottom")
<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 16 May 2019 07:46:38 GMT -->
</html>
