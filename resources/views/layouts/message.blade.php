<div class="row">
    <div class="col-md-12">
        @php
            $list = ['success','info','warning','danger','primary'];
        @endphp
        @foreach($list as $value)
            @if ( Session::has($value) )
                <div class="alert alert-{{ $value }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span>
                <b> {{ $value }} - </b> {{ Session::get($value) }}</span>
                </div>
            @endif
        @endforeach
    </div>
</div>
