@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @include('layouts.message')
            <div class="row">
                <div class="card">
                    <div class="card-content">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ $error }}</span>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">manage_accounts</i>
                            </div>
                            <h4 class="card-title">Company Export Management</h4>
                            <div class="pull-right">
                                @can('company-export-create')
                                    <a class="btn btn-success" href="{{ route('company-export.create') }}"> Create New Company Export </a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">


                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Name</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($companyExportList as $key => $company)
                                        <tr>
                                            <td class="text-center">{{ ++$i }}</td>
                                            <td>{{ $company->name }}</td>
                                            <td class="td-actions text-right">
                                                <button type="button" rel="tooltip" class="btn btn-info" onclick="window.location.href = '{{ route('company-export.show',$company->id) }}'">
                                                    <i class="material-icons">visibility</i>
                                                </button>
                                                @can('company-export-edit')
                                                <button type="button" rel="tooltip" class="btn btn-success" onclick="window.location.href = '{{ route('company-export.edit',$company->id) }}'">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                                @endcan
                                                <form method="post" class="delete-form"  style="display:inline" data-route="{{route('company-export.destroy',$company->id)}}">
                                                    @method('delete')
                                                <button type="submit" rel="tooltip" class="btn btn-danger" >
                                                    <i class="material-icons">close</i>
                                                </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('page_scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            $('.delete-form').on('submit', function(e) {
                e.preventDefault();
                var button = $(this);

                Swal.fire({
                    icon: 'warning',
                    title: 'Are you sure you want to delete this record?',
                    showDenyButton: false,
                    showCancelButton: true,
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if(result.value){
                        $.ajax({
                            type: 'delete',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:  button.data('route'),
                            data: {
                                '_method': 'delete'
                            },
                            success: function (response, textStatus, xhr) {
                                Swal.fire({
                                    icon: 'success',
                                    title: response,
                                    showDenyButton: false,
                                    showCancelButton: false,
                                    confirmButtonText: 'Yes'
                                }).then((result) => {
                                    window.location='{{ route('company-export.index',['success'=>'Company export deleted successfully.']) }}'
                                });
                            }
                        });
                    }else{
                        if(result.dismiss === "cancel"){

                        }
                    }
                });

            })
        });
    </script>
@endpush
