@extends('layouts.app')


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-content">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ $error }}</span>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 cols-sm-3 col-xs-12"></div>
                <div class="col-md-6 cols-sm-6 col-xs-12">
                    {!! Form::model($company, ['method' => 'PATCH','route' => ['company-export.update', $company->id],'enctype'=>'multipart/form-data']) !!}
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <a class="btn btn-primary" href="{{ route('company-export.index') }}">
                            <span class="btn-label">
                            <i class="material-icons">keyboard_arrow_left</i>
                            </span>
                                Back
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name" class="bmd-label-floating">Name</label>
                                {!! Form::text('name', null, array('id' => 'name','class' => 'form-control')) !!}
                                @error('name')
                                <label id="name-error" class="error" for="name">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="address" class="bmd-label-floating">Address</label>
                                {!! Form::text('address', null, array('id' => 'address','class' => 'form-control')) !!}
                                @error('address')
                                <label id="name-address" class="error" for="address">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="bmd-label-floating">Telephone</label>
                                {!! Form::text('telephone', null, array('id' => 'telephone','class' => 'form-control')) !!}
                                @error('telephone')
                                <label id="name-telephone" class="error" for="telephone">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group">
                                <h4 class="title">Logo</h4>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        @if($company->logo)
                                            <img src="{{ $company->logo->getFullUrl() }}" alt="{{ $company->logo->getCustomProperty('alt') }}">
                                        @else
                                            <img src="{{ asset('material-dashboard-pro/assets/img/image_placeholder.jpg') }}" alt="...">
                                        @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                  <span class="btn btn-rose btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                      {!! Form::file('logo', null, array('id' => 'logo')) !!}
                                  </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-3 cols-sm-3 col-xs-12"></div>
            </div>
        </div>
    </div>
@endsection
