@extends('layouts.app')
@push('page-link') <a class="navbar-brand" href="{{ route('company-export.index') }}">Company Export</a> @endpush
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <a class="btn btn-primary" href="{{ route('company-export.index') }}">
                            <span class="btn-label">
                            <i class="material-icons">keyboard_arrow_left</i>
                            </span>
                                Back
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                        <div class="card-body ">
                            <div class="form-group">
                                <label for="name" class="bmd-label-floating"> Name</label>
                                <input type="text" class="form-control" id="name" required="true" value="{{ $company->name }}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="code" class="bmd-label-floating"> Code</label>
                                <input type="text" class="form-control" id="code" required="true" value="{{ $company->code }}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="address" class="bmd-label-floating"> Address</label>
                                <input type="text" class="form-control" id="address" required="true" value="{{ $company->address }}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="bmd-label-floating"> Telephone</label>
                                <input type="text" class="form-control" id="telephone" required="true" value="{{ $company->telephone }}" disabled>
                            </div>

                            <div class="form-group">
                                <h4 class="title">Logo</h4>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{ $company->logo->getFullUrl() }}" alt="{{ $company->logo->getCustomProperty('alt') }}">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
