@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <a class="btn btn-primary pull-right" href="{{ route('roles.index') }}">
                                <span class="btn-label">
                                    <i class="material-icons">manage_accounts</i>
                                </span>
                                Back <div class="ripple-container"></div>
                            </a>
                        </div>
                        <div class="card-body ">
                            <div class="form-group">
                                <label for="exampleEmail" class="bmd-label-floating"> Name</label>
                                <input type="email" class="form-control" id="exampleEmail" required="true" value="{{ $role->name }}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="examplePassword1" class="bmd-label-floating"> Roles</label>
                                @if(!empty($rolePermissions))
                                    @foreach($rolePermissions as $v)
                                        <label class="label label-success">{{ $v->name }},</label>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
