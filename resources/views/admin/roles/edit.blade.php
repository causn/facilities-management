@extends('layouts.app')


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-content">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>{{ $error }}</span>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 cols-sm-3 col-xs-12"></div>
                <div class="col-md-6 cols-sm-6 col-xs-12">
                    {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">manage_accounts</i>
                            </div>
                            <h4 class="card-title">Edit Role</h4>
                            <a class="btn btn-primary pull-right" href="{{ route('roles.index') }}">
                            <span class="btn-label">
                            <i class="material-icons">keyboard_arrow_left</i>
                            </span>
                                Back
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                        <div class="card-body ">
                            <div class="form-group">
                                <label for="exampleName" class="bmd-label-floating">Name</label>
                                {!! Form::text('name', null, array('id' => 'exampleName','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleRole" class="bmd-label-floating">Permission</label>
                                @foreach($permission as $value)
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="permission[]" type="checkbox" value="{!! $value->id !!}" @php echo in_array($value->id, $rolePermissions) ? 'checked': ''; @endphp> {!! $value->name !!}
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer ">
                            <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-3 cols-sm-3 col-xs-12"></div>
            </div>
        </div>
    </div>
@endsection
