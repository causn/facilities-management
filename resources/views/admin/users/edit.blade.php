@extends('layouts.app')


@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                <button type="button" aria-hidden="true" class="close">
                                    <i class="material-icons">close</i>
                                </button>
                                <span>{{ $error }}</span>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 cols-sm-3 col-xs-12"></div>
            <div class="col-md-6 cols-sm-6 col-xs-12">
                {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
                <div class="card ">
                    <div class="card-header card-header-rose card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">person</i>
                        </div>
                        <h4 class="card-title">Edit New User</h4>
                        <a class="btn btn-primary pull-right" href="{{ route('users.index') }}">
                            <span class="btn-label">
                            <i class="material-icons">keyboard_arrow_left</i>
                            </span>
                            Back
                            <div class="ripple-container"></div>
                        </a>
                    </div>
                    <div class="card-body ">
                            <div class="form-group">
                                <label for="exampleName" class="bmd-label-floating">Name</label>
                                {!! Form::text('name', null, array('id' => 'exampleName','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleEmail" class="bmd-label-floating">Email</label>
                                {!! Form::text('email', null, array('id' => 'exampleEmail','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="examplePassword" class="bmd-label-floating">Password</label>
                                {!! Form::password('password', array('id' => 'examplePassword','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleEmail" class="bmd-label-floating">Confirm Password</label>
                                {!! Form::password('confirm-password', array('id' => 'exampleEmail','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleRole" class="bmd-label-floating">Role</label>
                                <input type="email" class="form-control" id="exampleRole" name="roles[]">
                                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'selectpicker','multiple','data-tpe'=>'select-with-transition','title'=>'Choose Role','data-size'=>7)) !!}
                            </div>
                    </div>
                    <div class="card-footer ">
                        <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-3 cols-sm-3 col-xs-12"></div>
        </div>
    </div>
</div>
@endsection
