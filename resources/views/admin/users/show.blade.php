@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-rose card-header-icon">
                            <a class="btn btn-primary pull-right" href="{{ route('users.index') }}">
                                <span class="btn-label">
                                    <i class="material-icons">keyboard_arrow_left</i>
                                </span>
                                Back <div class="ripple-container"></div>
                            </a>
                        </div>
                        <div class="card-body ">
                            <div class="form-group">
                                <label for="exampleEmail" class="bmd-label-floating"> Name</label>
                                <input type="email" class="form-control" id="exampleEmail" required="true" value="{{ $user->name }}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="examplePassword" class="bmd-label-floating"> Email</label>
                                <input type="text" class="form-control" id="examplePassword" required="true" name="password" value="{{ $user->email }}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="examplePassword1" class="bmd-label-floating"> Roles</label>
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $v)
                                        <label class="badge badge-success">{{ $v }}</label>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
